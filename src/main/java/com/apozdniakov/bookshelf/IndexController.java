package com.apozdniakov.bookshelf;


import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {
    @GetMapping(path = "/", produces = MediaType.TEXT_HTML_VALUE)
    public String indexPage() {
        return "<!DOCTYPE html><html><body><h1>Bookshelf app</h1><p>development in progress...</p></body></html>";
    }
}
